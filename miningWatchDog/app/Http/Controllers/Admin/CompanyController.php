<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['companies'] = Company::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.companies.index', $data);
    }

    public function create()
    {
        return view("admin.companies.create");
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'              => 'required',
            'description'       => 'required',
            'country'           => 'required',
            'website'           => 'required',
            'logo'              => 'required',
            'star_rating'       => 'required',
            'company_status'    => 'required',
            'note'              => 'required',
            'review_question'   => 'required',
            'legitimacy'        => 'required',

        ]);

        $company                    = new Company();
        $company->name              = $request->name;
        $company->description       = $request->description;
        $company->full_description  = $request->full_description;
        $company->country           = $request->country;
        $company->website           = $request->website;
        $company->star_rating       = $request->star_rating;
        $company->company_status    = $request->company_status;
        $company->note              = $request->note;
        $company->review_question   = $request->review_question;
        $company->legitimacy        = $request->legitimacy;

        if ($request->hasFile("logo") && $request->file('logo')->isValid()) {
            $filename           = $request->file('logo')->getClientOriginalName();
            $filename           = time()."-".$filename;
            $destinationPath    = public_path('/images');
            $company->logo      = "images/".$filename;
            $request->file('logo')->move($destinationPath, $filename);
        }

        $response = $company->save();

        if ($response) {
            return redirect()->route('admin.companies.index')->with("success", "Completed Successfully.");
        } else {
            return redirect()->back()->withInput($request->all())->with("error", "Something went wrong. Please try again.");
        }
    }

    public function show(Company $company)
    {
        $data['company'] = $company;
        return view('admin.companies.show', $data);
    }

    public function edit(Company $company)
    {
        $data['company'] = $company;
        return view('admin.companies.edit', $data);
    }


    public function update(Request $request, Company $company)
    {
        $request->validate([
            'name'          => 'required',
            'description'   => 'required',
            'country'       => 'required',
            'website'       => 'required',
            'logo'          => 'required',
            'star_rating'   => 'required',
            'company_status' => 'required',
            'note'          => 'required',
            'review_question' => 'required',
            'legitimacy'    => 'required',

        ]);

        $company->name          = $request->name;
        $company->description   = $request->name;
        $company->country       = $request->name;
        $company->website       = $request->name;
        $company->logo          = $request->name;
        $company->star_rating   = $request->name;
        $company->company_status= $request->company_status;
        $company->note          = $request->note;
        $company->review_question = $request->review_question;
        $company->legitimacy    = $request->legitimacy;

        $response = $company->save();

        if ($response) {
            return redirect()->route('admin.companies.index')->with("success", "Completed Successfully.");
        } else {
            return redirect()->back()->withInput($request->all())->with("error", "Something went wrong. Please try again.");
        }
    }


    public function destroy(Company $company)
    {
        $company->active = $company->active == 0 ? 1 : 0;
        $response = $company->save();

        if ($response) {
            return redirect()->back()->with("success", "Completed Successfully.");
        } else {
            return redirect()->back()->with("error", "Something went wrong. Please try again.");
        }
    }
}
