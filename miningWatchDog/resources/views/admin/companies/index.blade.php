@extends('admin.layouts.master')

@section('content')

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="javascript:;">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Company</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right open">
                <a href="{{ route('admin.companies.create') }}" class="btn blue btn-sm" > <b><i class="fa fa-plus"></i> Add</b></a>
            </div>
        </div>

    </div>
    <h3 class="page-title">Company
    </h3>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit bordered">

                <div class="portlet-body flip-scroll">
                    <table class="table table-bordered table-striped flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th width="75px"> Sr No. </th>
                            <th> Company Name </th>
                            <th> Country </th>
                            <th> Website </th>
                            <th> Rating </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i = 0;
                        @endphp
                        @forelse($companies as $company)
                            <tr class="table-row-clickable" onclick="window.location = '{{ route('admin.companies.show', $company->id) }}'">
                                <td> {{ ++$i }} </td>
                                <td> {{ $company->name }} </td>
                                <td> {{ $company->country }} </td>
                                <td> {{ $company->website }} </td>
                                <td> {{ $company->star_rating }} </td>
                                <td> {{ $company->active == 1 ? 'Active' : 'Inactive'}} </td>
                                <td>
                                    <form action="{{ route('admin.companies.destroy',$company->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-primary btn-sm" href="{{ route('admin.companies.edit', $company->id) }}">Edit</a>
                                        @if($company->active == 0)
                                            <button type="submit" class="btn btn-success btn-outline btn-sm sbold uppercase">Active</button>
                                        @else
                                            <button type="submit" class="btn btn-danger btn-outline btn-sm sbold uppercase">Inactive</button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">
                                    Data Not Found
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{$companies->links()}}
                    </div>
                </div>
            </div>
        </div>
@endsection

