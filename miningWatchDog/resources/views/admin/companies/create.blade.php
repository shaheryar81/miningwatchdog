@extends('admin.layouts.master')

@section('content')

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="javascript:;">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Projects</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Create</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right open">
                <a href="{{ url()->previous() }}" class="btn red btn-sm" > <b><i class="fa fa-backward"></i> Back</b></a>
            </div>
        </div>

    </div>
    <h3 class="page-title">Projects
        <small>Create Project</small>
    </h3>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered">

                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{ route('admin.companies.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">

                                    <div class="form-group form-md-line-input">
                                        <input type="text" name="name" class="form-control" id="form_control_1" placeholder="Enter company name" value="{{old('name')}}">
                                        <label>Company Name</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="text" name="website" class="form-control" id="form_control_1" placeholder="Enter website name" value="{{old('Website')}}">
                                        <label>Website Name</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="text" name="country" class="form-control" id="form_control_1" placeholder="Enter country name" value="{{old('country')}}">
                                        <label>Country Name</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="file" name="logo" class="form-control" id="form_control_1" value="{{old('logo')}}">
                                        <label>Add Logo</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="text" name="description" class="form-control" id="form_control_1" placeholder="Enter full description" value="{{old('full_description')}}">
                                        <label>Description</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="number" name="star_rating" class="form-control" id="form_control_1" placeholder="Enter star rating" value="{{old('star_rating')}}">
                                        <label>Star Rating</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="text" name="review_question" class="form-control" id="form_control_1" placeholder="Enter Review Question" value="{{old('review_question')}}">
                                        <label>Review Question</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <input type="text" name="note" class="form-control" id="form_control_1" placeholder="Enter note" value="{{old('note')}}">
                                        <label>Note</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <select class="form-control" name="company_status">
                                            <option value="">Company Status</option>
                                            <option value="approve" selected>Legitimate Company and Mining WatchDog Approved.</option>
                                            <option value="scam">Scam Company and Not Approved By Mining WatchDog.</option>
                                            <option value="not_sure">Not sure, probably best to stay clear of this company.</option>
                                        </select>
                                        <label>Company Status</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <select class="form-control" name="legitimacy">
                                            <option value="">Legitimacy</option>
                                            <option value="green" selected>Legitimate Company and Mining WatchDog Approved.</option>
                                            <option value="yellow">Scam Company and Not Approved By Mining WatchDog</option>
                                            <option value="red">Not sure, probably best to stay clear of this company.</option>
                                        </select>
                                        <label>Legitimacy</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <select class="form-control" name="active">
                                            <option value="">Status</option>
                                            <option value="1" selected>Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                        <label>Status</label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <textarea name="full_description" class="summernote" placeholder="Description"></textarea>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <input type="submit" class="btn green" value="Submit">
                                    <input type="reset" class="btn default" value="Reset">
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
@endsection
